package com.example.demo.repositories

import com.example.demo.models.Persona
import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

@Repository
interface PersonaRepository : JpaRepository<Persona, Int> {
	
	fun findByNombre(nombre: String): Optional<Persona>
	
}