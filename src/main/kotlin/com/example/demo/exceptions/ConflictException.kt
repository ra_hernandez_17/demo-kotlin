package com.example.demo.exceptions

import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.http.HttpStatus

@ResponseStatus(HttpStatus.CONFLICT)
class ConflictException : RuntimeException {

	constructor(codigo: String?) : super("Hay un conflicto con el registro " + codigo)

}