package com.example.demo.controllers

import com.example.demo.repository.EntityService
import com.example.demo.models.Persona
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import com.example.demo.exceptions.NotFoundException
import org.springframework.http.HttpStatus
import java.net.URI
import com.example.demo.exceptions.ConflictException
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Api
import io.swagger.annotations.ApiResponses
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/persona")
@Api(
	value = "Sistema de gestión de Personas",
	description = "Operaciones CRUD relacionadas con las Personas en el sistema de gestión de Personas"
)
@CrossOrigin("*")
class PersonaController {

	@Autowired
	lateinit var personaService: EntityService<Persona, Int>

	@GetMapping
	@ApiOperation(value = "Ver una lista de Personas", response = ArrayList::class)
	@ApiResponses(
		ApiResponse(code = 200, message = "Lista Recuperada")
	)
	fun getAll(): ResponseEntity<Any> {
		return ResponseEntity.ok(personaService.getAll())
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Ver una Persona por su Id", response = Persona::class)
	@ApiResponses(
		ApiResponse(code = 200, message = "Persona Recuperada"),
		ApiResponse(code = 404, message = "Persona no encontrada")
	)
	fun getById(
		@ApiParam(value = "ID de la persona a recuperar", required = true)
		@PathVariable id: Int
	): ResponseEntity<Any> {
		return ResponseEntity.ok(personaService.getById(id))
	}

	@PostMapping
	@ApiOperation(value = "Guardar una Persona")
	@ApiResponses(
		ApiResponse(code = 200, message = "Persona Recuperada"),
		ApiResponse(code = 409, message = "Conflicto entre ID existentes")
	)
	fun save(
		@ApiParam(value = "Datos de la Persona a guardar", required = true)
		@RequestBody persona: Persona
	): ResponseEntity<Any> {
		if (personaService.exist(persona.idPersona)) {
			throw ConflictException(persona.idPersona.toString())
		}
		personaService.save(persona)
		return ResponseEntity
			.created(URI("/api/" + persona.idPersona)).body("")
	}

	@ApiOperation(value = "Editar una Persona")
	@ApiResponses(
		ApiResponse(code = 200, message = "Persona Modificada"),
		ApiResponse(code = 404, message = "Persona no encontrada"),
		ApiResponse(code = 409, message = "Conflicto con ID no concidiente de persona a editar")
	)
	@Throws(NotFoundException::class, ConflictException::class)
	@PutMapping("/{id}")
	fun modify(
		@ApiParam(value = "ID de la persona a editar", required = true)
		@PathVariable id: Int,
		@ApiParam(value = "Datos de la Persona a editar", required = true)
		@RequestBody persona: Persona
	): ResponseEntity<Any> {
		if (!personaService.exist(id)) {
			throw NotFoundException(id.toString())
		}
		if (!id.equals(persona.idPersona)) {
			throw ConflictException(id.toString())
		}
		personaService.save(persona);
		return ResponseEntity.ok(URI("/api/" + persona.idPersona))
	}

	@ApiOperation(value = "Eliminar una Persona")
	@ApiResponses(
		ApiResponse(code = 200, message = "Persona Eliminada"),
		ApiResponse(code = 404, message = "Persona no encontrada")
	)
	@Throws(NotFoundException::class)
	@DeleteMapping("/{id}")
	fun delete(
		@ApiParam(value = "ID de la persona a eliminar", required = true)
		@PathVariable id: Int): ResponseEntity<Any> {
		if (!personaService.exist(id))
			throw NotFoundException(id.toString())
		personaService.delete(id);
		return ResponseEntity(HttpStatus.OK)
	}

}