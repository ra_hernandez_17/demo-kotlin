package com.example.demo.config

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2
import org.springframework.context.annotation.Configuration
import springfox.documentation.service.Contact


@Configuration
@EnableSwagger2
open class Swagger2Config {

	@Bean
	open fun api(): Docket {
		return Docket(DocumentationType.SWAGGER_2).select()
			.apis(RequestHandlerSelectors.basePackage("com.example.demo.controllers")).paths(
				PathSelectors.regex("/.*")
			).build().apiInfo(apiInfo())
	}

	private fun apiInfo(): ApiInfo {
		return ApiInfoBuilder().title("Reservas API").description("ReservasAPI reference for developers")
			.termsOfServiceUrl(
				"http://www.javainuse.com"
			).license("MIT License")
			.contact(Contact("Raul Hernandez", "https://springframework.guru/about/", "raulandres462@gmail.com"))
			.version("1.0").build();
	}

}